
Access
==========================================

Authentication
~~~~~~~~~~~~~~~~~~~

When you sign up for the API you will be given a username and
API key which will be on your settings page.

Add the username and key *either* in an Authorization header *or* to GET/POST paramaters::


    #if username is 'a' and api_key is 'b': 
    #as a header
    Authorization: ApiKey a:b
    # As GET params (e.g in brower, if using a browser also add &format=json or &format=xml)
    /api/resource/url/?username=a&api_key=b
    

Sandbox
~~~~~~~~~~~~~~~~~~~~

You'll start off in Sandox mode. On the settings page add up to 5 emails where you
will be sending your test documents.

Once you're ready for production let us know and we'll switch you over, at which point you'll be
able to send documents to any email.

In Sandbox mode if you try and send a document to someone not on your sandbox email list you'll get
back a 401 Not Authorized response.


Authorization
~~~~~~~~~~~~~~~~~~~

API Keys are provided on a per-user basis. API access will have the same level of authorization
as the user in the web interface. The user will be able to access the templates, documents and members of all the
groups to which the API user belongs.

We recommend you create a non-specific 'master' user for your firm and then add individual member accounts to each group.
Use the 'master' for API access, to sign up to service plans and create groups. This way
you do not tie control of your groups to an individual, you ensure that you
do not buy mutliple plans for different sets of groups, and you get better value as you
add more groups.


API URL
~~~~~~~~~~~~~~~~~~~~

The root url for all API requests is: https://legalesign.com/api/v1/


Format
~~~~~~~~~~~~~~~~~~~~

Use JSON or XML, defaults to JSON. If you make an API query in the browser you
must add ?format=[json,xml] to the querystring.
 
* GET requests - the Accept header or append format=[json,xml] to the querystring
* POST, PATCH, DELETE requests - use the Content-Type header


    #Accepts header in curl
    curl -H "Accept: application/xml" -X GET https://legalesign..com/api/v1/
    
    #Content-Type for sending data to the server
    curl -H "Content-Type: application/json" -X POST -- data {...} https://legalesign..com/api/v1/

    #querystring
    /api/resource/url/?format=json
    

    
Throttling
~~~~~~~~~~~~~~~

The price plan for the API user will define hourly throttle limits, or the limits will have been agreed with us separately.

You will find your current throttle limit on the API user's settings page (once you have registered for an API key)

If you start exceeding your throttling limits but are delayed in upgrading (waiting for payment authorization etc)
let us know so we can temporarily increase your request limits to ensure consistent service.

A throttled request will return HTTP code 429.

Callback notifications are not included in throttle limits.

You can check your current status by calling the /throttle/ endpoint. It will return your limit and requests in the last hour.

For example, request in curl::
    
    curl -H "Authorization: ApiKey f6009234a674463d8d2def328b4321:a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0"
    https://legalesign.com/api/v1/throttle/

Will respond like::

     {"limit": 100, "requests_in_last_hour": 22}

