
Get started
==========================================

A common use-case is to send out a document
to be signed, get notified when the document is fully executed and pull down the
final pdf.



Get authorized
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First, contact us for an API key: `Contact us <https://legalesign.com/opn/contact/>`_

Let us know what you want to do and expected throughput.

We'll assign you an API key which you can find on your settings page.

To start with you'll be in Sandbox mode. On the settings page add up to 5 emails which will be
the test signers to whom you want to send documents.
If you send a document to someone else you will get a 401 Not Authorized response.




Send a doc to be signed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Once you have your API key you can start sending documents right away. Legalesign
has a 'template' system so you can refer to a doc you saved on Legalesign earlier (text or pdf), or you can send HTML
down the pipe directly. Sending HTML is most popular since it gives you the total freedom in the construction of each document you send. We'll use curl to illustrate requests to the server::

    curl --dump-header -
    -H "Authorization: ApiKey f6009234a674463d8d2def328b4321:a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0"
    -H "Content-Type: application/json"
    -X POST --data '{
    "group": "/api/v1/group/mygroup/",
    "name": "Name of doc",
    "text": "<h1>Non disclosure agreement</1><p>terms as follows..</p>",
    "signers":
        [{"firstname": "Joe",
        "lastname": "Blogs",
        "email": "email@legalesign.com",
        "order": 0
        }],
    "do_email": "true"
    }'
    https://legalesign.com/api/v1/document/

ApiKey credentials are put in an Authorization header, we specify
the content type for the data we're POSTing, and push the data to the /document/ endpoint.

In this example we're sending out a short document directly (using the 'text'
attribute), to have it signed by Joe Bloggs <email@legalesign.com>. 

The server's response to our POST includes a Location header with the status resource_uri for the new document::

    Location: https://legalesign.com/api/v1/status/51361087-ce3c-4eae-bb26-d77712fd9374/
    

Get the doc status
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can poll the API for the document status using the /status/<document_uuid>/ endpoint (this is also the Location
header that is returned in the 201 response when the document is successfully created)::

    curl --dump-header - -H "Authorization: ApiKey f6009234a674463d8d2def328b4321:
    a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0" -H "Content-Type: application/json"
    -X GET https://legalesign.com/api/v1/status/51361087-ce3c-4eae-bb26-d77712fd9374/
    

This returns a short dictionary that lets us know the document status::

    {
    "archived": false, 
    "resource_uri": "/api/v1/status/51361087-ce3c-4eae-bb26-d77712fd9374/", 
    "status": 30,
    "download_final: true,
    }
    
In this case '30' indicates the document has been signed - :ref:`Document status codes <document_status_codes>`
'download_final' confirms that the final document has been created and is available for download.

If you want more document attributes use the /document/<document_uuid>/ endpoint (uuid refers to a string
of letters and numbers that are used identify an individual object). The /document/
endpoint includes individual signer status, amongst other things::

    
    {
    "archived": false,
    "created": "2013-03-25T15:54:11",
    "footer": "",
    "group": "/api/v1/group/mygroup/",
    "has_fields": true,
    "download_final": false,  //bool to indicate final PDF download is available
    "hash_value": null,  //the hash of the final pdf (when signed)
    "modified": "2013-03-25T15:54:54",
    "name": "opt1",
    "resource_uri": "/api/v1/document/51361087-ce3c-4eae-bb26-d77712fd9374/",
    "sign_time": null,
    "signature_placement": 2, //1 = place on each page, 2= place at end of doc
    "signature_type": 1, //1 = basic signature, 2 = advanced esignature
    "signers": [
      [
         "api/v1/user/2c7f7ac4-7226-4608-b2aa-765fca2473f5",
        "api/v1/signer/2c7f7ac4-7226-4608-b2aa-765fca2473f5", 
       "behalf of",
        true, //whether this signer has fields to complete
        30,   //signer status
        0     //signer order (zero-indexed)
      ]
    ],
    "status": 20,
    "text": "<h1>Non disclosure agreement</1><p>terms as follows..</p>",
    "user": "/api/v1/user/f6009234a674463d8d2def328b4321/",
    "uuid": "71c3b1b1-8aa1-4719-89f6-0c7126c6830b"
    }
    

Notification upon signing
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Rather than make repeated queries for status you can define your own URL endpoint
and we'll POST notifications to you. There are two notification systems, either you can
elect to be notified when a document is signed, or you can get all changes of every kind (document visited, fields
completed etc. for both document and signer) every 6 minutes. These notifications are not
included in API throttle limits and so they are preferable to polling.

Once we've confirmed your URL you'll start getting notifications.

In this case we've chosen to receive all information every 6 minutes. Here's an example of data sent to your notifiation URL::

    {'data': [u'[{"timestamp": "2013-04-21T13:47:32.624100",
   "name": "status", "value": "30",
    "resource_uri": "/api/v1/document/4a315cfe-1731-484c-9d3b-b10b68168d42/"}]'],
    u'signed': [u'R3F3R1FuOHVwaVpTT3VtOUl0dlFBTzdHMDBxeDVhSEV2MkkyeGF5LzVGWEJ0Y
    ytEYjdxQ3JyZWl0RnBHVU92UnRPaHljVGtxazExeXJ4N0t1Z0MvUk5UckM3S25HWmlTcXhha1ky
    OU4ra203YWdKV09YUFdYSk4rRXZ4T2lQc3FkU25KQzgzNEs2bkoyVWE5NUhJUW1CWUd4WU1iQjR
    2UFZvdnRQUXJtZlpRbTZvM1l6aFJpci9wWU0rQzdsaFh0RTBwaHBjOGVyM0t2cHMyTlV2V2VjW
    DV6OE81UlhtQmpkTVZjMFQzWU9rbzA0VTI5Mm0ySGJTbE5CVDd1WDZEMGdWN3UvYVQvMU1lcC9
    0U3JmWFVoUTBFdUFSazVEOWhHVFJmb3owQm41cURTSmxxU1JCdHhYa1JNVXBld1NDUnhSMitYV
    EZjUkdxUVl2bm1UZjQ1N2hNbXpvMnA2NnhJRTlMbmtubFJaYW1HNWVWV0NPT256RWhYY2JvRHR
    nOU04VFgvbTJWUUVqVklZU0QxRUpWdE83S2xpSzBLdjBvbG5JWTVHWTQwVU14aEIyeGpGY0JsN
    mJuK0J1OFBBcXFBTGFLTzRKK3pwcjRJUDJueU10Q0xWQXB4WC8xOFhSUDhMSE1jOW9QcUxIVnF
    3L0c2aW5uZFRQVlB6d3hjaTZnWldNcUtLdUQ5UXdrOFRiL2tObUgxNkN2VUh6M3BlSjd6TEdKd
    UdPazk1Vm5sNmcyV01zeGpvTS80citlSW0xRkpOYzVKU3B3ay9zWTNZcVdMaFNhT0ovV0dORHd
    SOUFWamZrYkQyKzN0dDR2WTh6U095ZHc5YjFHeUZWSVRtL0YzTmVpZVRFRDdqVThvak1zc0Rac
    k9waUVmaVRBV2NteWNKY3Njd25zNFZEbFE9']}
    
    
In this case the document at '/api/v1/document/51361087-ce3c-4eae-bb26-d77712fd9374/'
status has been changed to 30, i.e. signed.

(For document status codes see: :ref:`Document status codes <document_status_codes>` )

Notifications include both signer and document changes (thus the final signing on a
doc will include at least two data dictionaries for that doc - one for the signer and one for the document).
If the notification
refers to a signer then 'resource_uri', will point to a signer resource, e.g.
'/api/v1/signer/2c7f7ac4-7226-4608-b2aa-765fca2473f5/' and if it refers to a document
the resource_uri will refer to the document, e.g. '/api/v1/document/2c7f7ac4-7226-4608-b2aa-765fca2473f5/'.

The 'signed' part of the data is a base64 encoded signed string you can use along with the Legalesign
x509 certficate to verify we really sent the data, should you wish to double check.

For more information on callack go to :ref:`Document Callback <document_callback>`.


Get the final PDF
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Once a document is signed make a GET request to the pdf endpoint to pull down the signed pdf::

    curl --dump-header -o download.pdf - -H "Authorization: ApiKey f6009234a674463d8d2def328b4321:
    a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0" 
    -X GET https://legalesign.com/api/v1/pdf/51361087-ce3c-4eae-bb26-d77712fd9374/
    
If you wish to verify the download, the SHA-256 hash value for the signed pdf is available from the document endpoint
(i.e https://legalesign.com/api/v1/document/<document_uuid>/)

There can be a lag between signing and final PDF creation. To make sure the download is
available use either (1 ) 'document_final' bool provided in the /document/
or /status/ endpoints or (2) code '100' in the callback
system. 



Finishing up
~~~~~~~~~~~~~~~~~~

Since auto_archive is set to true by default when we originally sent the POST request earlier, the
document will get sent to the archive automatically once it is signed.

This prevents the web interface from building up clutter if it is rarely being used.

But if you set auto_archive to false, then you can finish up by archiving the document, perhaps after a given time lapse::

    curl --dump-header - -H "Authorization: ApiKey f6009234a674463d8d2def328b4321:
     a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0" -H "Content-Type: application/json"
     -X PATCH --data '{ "archived": true }'
     https://legalesign.com/api/v1/archived/51361087-ce3c-4eae-bb26-d77712fd9374/
     

If you have any questions or technical issues whilst testing your integration please
contact us at support@legalesign.com, we look forward to hearing from you.