.. legalesign api documentation master file, created by
   sphinx-quickstart on Sun Apr 14 13:14:22 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Legalesign API
==========================================

`Legalesign.com <https://legalesign.com/>`_ is an online eSignature service. This is the documentation for its
REST-style API.

Legalesign Esign API integration is cost-effective against developing bespoke solutions and
is recommended over in-house systems to mitigate accusations of document tampering.

Legalesign is specifically designed with API usage in mind.

* text/html based templating system that provides a richer scope for document integration (though executed documents are still converted to PDF), and;
* Legalesign is structured around groups - simple to implement but with the flexibility to describe many organisational systems; and
* accessible and affordable REST-style API.
* excellent value for money


In a hurry? Go to `quick start - code samples <http://legalesign-api.readthedocs.org/en/latest/start_php.html>`_.


    "Using the Legalesign API is a breeze, sending and processing e-signed documents couldn't be easier"
     
    -- James, Director of Information Systems @ Ready Project, www.readyproject.com

    

    "I am definitely planning on using the Legalesign API again soon. I appreciated the support and attention to security."

    -- Arthur King, Head of IT, N4Gtv.com


For more information about Legalesign, pricing information, and to try Legalesign in a web interface go to: https://legalesign.com/

For technical assistance please email us at support@legalesign.com and we'll be pleased to help out.

If you're seeking to integerate Legalesign with another app (basecamp, freeagent, gmail etc) first check out `Zapier.com <https://zapier.com/>`_.
Zapier enables you
to integrate apps without coding. Use this link to join Legalesign on Zapier: https://zapier.com/developer/invite/3361/8d004571f8382b6a5f098897d2701856/

Contents:

.. toctree::
   :maxdepth: 2

   start
   start_php
   access
   general
   documents
   callback
   templates
   users
   groups
   members
    

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

