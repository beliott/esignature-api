
Templates
==========================================

Templates are re-usable stored documents.  Version control of templates is not available in this API version.

POST, PATCH attributes for a template

============ ========  ======= ============ ==========
attribute    required  default type         max length
============ ========  ======= ============ ==========
user         Y                 resource_uri
group        Y                 resource_uri
title        Y                 string       60
latest_text  Y                 string
signee_count N         2       integer
archive      N         False   bool
comment*     N                 string       250
============ ========  ======= ============ ==========

\*comment is used for version control and can be added when sending data to server, but it is not returned in GET requests.



template/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Templates to which the user has access. By default only unarchived templates will
be returned, you can use the filters below to return archived or all documents

Allowed methods: GET, POST

Filters - use in the querystring to filter by archive status or group:

* archive, e.g. archive=all, archive=true, archived=false. default archive=false
* group, e.g. group=mygroup or group=/api/v1/group/mygropu (by url name or resource_uri)


Example of a GET response in JSON::

    {
      "meta": {
        "limit": 20, 
        "next": null, 
        "offset": 0, 
        "previous": null, 
        "total_count": 2
      }, 
      "objects": [
            {
              "archive": false, 
              "created": "2013-01-14T18:51:06", 
              "group": "/api/v1/group/mygroup/", 
              "has_fields": false, 
              "modified": "2013-04-13T10:50:27", 
              "resource_uri": "/api/v1/template/4139f5ae-17bd-4b7e-a56a-299f060539df/", 
              "signee_count": 2, 
              "title": "Doc Title", 
              "uuid": "4139f5ae-17bd-4b7e-a56a-299f060539df"
            }, 
            {
              "archive": false, 
              "created": "2013-01-11T17:46:20", 
              "group": "/api/v1/group/secondgroup/", 
              "has_fields": false, 
              "modified": "2013-04-13T10:50:27", 
              "resource_uri": "/api/v1/template/e6697544-6f47-4b72-9bc6-fba545f860de/", 
              "signee_count": 2, 
              "title": "Doc Title 1", 
              "uuid": "e6697544-6f47-4b72-9bc6-fba545f860de"
           }
        ]
      }
          

Example of a POST using curl::

    curl --dump-header - -H "Content-Type: application/json"
    -H "Authorization: ApiKey f6009234a674463d8d2def328b4321:a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0"
    -X POST --data '{ "user": "/api/v1/user/f6009234a674463d8d2def328b4321/", "group": "/api/v1/group/mygroup/",  "latest_text": "[text here]",
    "title": "a title",  "comment": "initial draft" }' https://legalesign.com/api/v1/template/



template/<template_uuid>/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A single template, includes the latest text.

Allowed methods: GET, PATCH, DELETE

Example of a GET response in JSON::

    {
      "archive": false, 
      "created": "2013-01-14T18:51:06", 
      "group": "/api/v1/group/mygroup/", 
      "has_fields": false, 
      "latest_text": "<p>text here</p>", 
      "modified": "2013-04-13T10:50:27", 
      "resource_uri": "/api/v1/template/4139f5ae-17bd-4b7e-a56a-299f060539df/", 
      "signee_count": 2, 
      "title": "Doc title", 
      "uuid": "4139f5ae-17bd-4b7e-a56a-299f060539df"
    }


Example of a PATCH using curl::

    curl --dump-header - -H "Content-Type: application/json"
    -H "Authorization: ApiKey f6009234a674463d8d2def328b4321:a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0" -X PATCH
    --data '{ "user": "/api/v1/user/f6009234a674463d8d2def328b4321/", "group": "/api/v1/group/mygroup/",
    "latest_text": "[doc text here]", "title":"doc title"}' https://legalesign.com/api/v1/template/f010ce7c-0f2e-4099-a27d-f1501f30419b/
