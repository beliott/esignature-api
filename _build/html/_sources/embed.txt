
Embed signing pages
==========================================


You can put the signing pages within an iframe on your own site.

Send us the URL you want to use. It needs to be able to take 2 dynamic variables
e.g. yoursite.com/VAR1/VAR2/ or yoursite.com/sign?A=VAR1&B=VAR1.

Once you approve this all signers will redirect to your URL. When your
URL is clicked you need to do a few things with VAR1 and VAR2 along with
your API key to create a signed IFRAME src.

1. Extract the VAR1 and VAR2 from the URL.

2. Create the following url string:
'/esign/i/$VAR1/$VAR2/'

3. Construct the following string:
URL=[url from 2]&Timestamp=[seconds_since_epoch]&Username=[api username]&Operation=Iframe
(Use exactly that order and capitalization.)

4. Using the string from 3:
a) URLEncode it
b) HMAC hash it with SHA-256 and your Api key.
c) Base64 encode it.

5. Finally Construct your iframe src:
https://legalesign[url from 2]?Timestamp=[timestamp from 3]&Username=[api username]&Signature=[string from 4].



Here is an example in PHP::

    $api_username = ‘xxxx’;
    $api_key = ‘zzzz’;
    $timeStamp = time();
    $urlparts = explode("/", $url);
    $signerId = $urlparts[6];
    $tokenId = $urlparts[7];
    $signature_url = "/esign/i/".$signerId."/";
    $signature = urlencode('URL='.$signature_url."&Timestamp="$timeStamp."&Username=".$api_username."&Operation=Iframe");
    $signature_key = hash_hmac('sha256', $signature, $api_key);
    $signature_key = base64_encode($signature_key);
    $finalUrl = " https://legalesign.com".$signature_url."Timestamp=".$timeStamp."&Username=".$api_username."&Signature=".$signature_key;
    return $finalUrl;


Here is an example in Python::
    
    import hashlib
    import hmac
    import urllib
    import base64
    import time
    
    #parse id and token from URL
    
    url = '/esign/i/%s/%s/' % (id, token)
    signing_vars = {
        'url': url,
        'tstamp': time.time(),
        'username': 'xxxxx',
    }
    to_sign = "URL=%(url)s&Timestamp=%(tstamp)s&Username=%(username)s&Operation=Iframe" % signing_vars
    to_sign = urllib.quote_plus(request)
    signed = hmac.new(str('api key'), to_sign, hashlib.sha256)
    signed =  base64.b64encode(signed.hexdigest())
    signing_vars['signed'] = signed
    return 'https://legalesign.com%(url)s?Timestamp=%(tstamp)s&Username=%(username)s&Signature=%(signed)s' % signing_vars
    